﻿#region USING_DIRECTIVES

#endregion

namespace LaReine.Web.Common.Configuration {
    public class AppOptions {
        public LoggingOptions Logging { get; set; }

        public AppOptions(bool isGenerating = false) { 
            if (isGenerating) {
                Logging = new LoggingOptions();
            }
        }
    }
}
