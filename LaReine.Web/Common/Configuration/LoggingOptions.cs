﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace LaReine.Web.Common.Configuration {
    public class LoggingOptions {
        public LoggingOptions Default => new LoggingOptions {
            UseRelativeOutput = true,
            OutputDirectory = "Logging",
            DateTimeFormat = "yyyy-MM-dd",
            MaxFileSizeKb = 5000,
            Output = LogLevel.Information
        };

        public bool UseRelativeOutput { get; set; }
        public string OutputDirectory { get; set; }
        public string DateTimeFormat { get; set; }
        public int MaxFileSizeKb { get; set; }
        public LogLevel Output { get; set; }
    }
}
