#region USING_DIRECTIVES
using Microsoft.AspNetCore.Mvc;
#endregion USING_DIRECTIVES

namespace LaReine.Web.Controllers {
    public class HomeController : Controller {
        public IActionResult Index() {
            return View();
        }
    }
}