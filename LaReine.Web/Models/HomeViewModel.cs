#region USING_DIRECTIVES
using RedditSharp;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using LaReine.Web.Common.Configuration;
#endregion

namespace LaReine.Web.Models {
    public class HomeViewModel {

        public IConfiguration _config { get; private set; }
        public string Title { get; internal set; }

        internal Task KaprisoftReddit() {
            var reddit = new Reddit();
            var user = reddit.LogIn(_config["reddit:username"], _config["reddit:password"]);
            var sub = reddit.GetSubreddit("/r/kaprisoftstudios");
            sub.Subscribe();

            return Task.CompletedTask;
        }

        public HomeViewModel Default => new HomeViewModel {
            Title = ""
        };
    }
}