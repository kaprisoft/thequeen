﻿#region USING_DIRECTIVES
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using LaReine.Web.Common.Configuration;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;
#endregion

namespace LaReine.Web {
    public class Program {

        public static IConfiguration Config { get; internal set; }

        public static void Main(string[] args) {
            TryGenerateConfig();

            var builder = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddYamlFile("_config.yaml")
                .Build();
            Config = builder;

            CreateWebHostBuilder(args).UseConfiguration(Config).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) {

            return WebHost.CreateDefaultBuilder(args)
            .UseStartup<Startup>();
        }

        public static bool TryGenerateConfig()
        {
            var filepath = Path.Combine(AppContext.BaseDirectory, "_config.yaml");
            if (File.Exists(filepath)) return false;

            var serializer = new SerializerBuilder()
                .WithNamingConvention(new UnderscoredNamingConvention())
                .Build();

            var yaml = serializer.Serialize(new AppOptions(true));
            File.WriteAllText(filepath, yaml);
            return true;
        }
    }
}
