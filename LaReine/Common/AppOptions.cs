﻿#region USING_DIRECTIVES

using Discord;

#endregion
namespace LaReine.Common
{
    public class AppOptions
    {
        public string SuccessEmoji { get; set; } = ":thumbs_up:";
        public DiscordOptions Discord { get; set; }
        public LoggingOptions Logging { get; set; }

        public AppOptions(bool isgenerating = false)
        {
            if (isgenerating)
            {
                Discord = new DiscordOptions();
                Logging = new LoggingOptions();
            }
        }
    }
}
