﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LaReine.Common
{
    public class DiscordOptions
    {
        public string Prefix { get; set; } = "";
        public string Token { get; set; } = "";
    }
}
