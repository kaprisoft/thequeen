﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LaReine.Common
{
    public class LoggingOptions
    {
        public LoggingOptions()
        {
            UseRelativeOutput = true;
            OutputDirectory = "Logging";
            DateTimeFormat = "yyyy-MM-dd";
            MaxFileSizeKb = 5000;
        }

        public bool UseRelativeOutput { get; set; }
        public string OutputDirectory { get; set; }
        public string DateTimeFormat { get; set; }
        public int MaxFileSizeKb { get; set; }
    }
}
