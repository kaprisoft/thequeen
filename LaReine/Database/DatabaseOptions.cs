﻿#region USING_DIRECTIVES
using static LaReine.Database.DatabaseContextBuilder;
#endregion

namespace LaReine.Database
{
    public class DatabaseOptions
    {
        public DatabaseOptions DbConfig { get; set; }

        public DatabaseProvider Provider { get; set; }

        public string DatabaseName { get; set; }

        public string Hostname { get; set; }

        public string Password { get; set; }

        public int Port { get; set; }

        public string Username { get; set; }

        public DatabaseOptions Default => new DatabaseOptions {
            DbConfig = DbConfig.Default,
            Provider = DatabaseProvider.PostgreSQL,
            DatabaseName = "LRDB",
            Hostname = "localhost",
            Password = "lrdb1",
            Port = 5432,
            Username = ""
        };
    }
}