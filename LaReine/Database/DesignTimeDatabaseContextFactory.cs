﻿#region USING_DIRECTIVES

using Microsoft.EntityFrameworkCore.Design;

using YamlDotNet;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

using System.IO;
using System.Text;
using System.Threading.Tasks;

using LaReine.Common;

#endregion

namespace LaReine.Database
{
    public class DesignTimeDatabaseContextFactory : IDesignTimeDbContextFactory<DatabaseContext>
    {
        private DatabaseOptions Dbconfig { get; set; }

        public DatabaseContext CreateDbContext(params string[] args)
        {
            DatabaseOptions cfg = Dbconfig.Default;
            string yaml = "";
            var utf8 = new UTF8Encoding(false);
            var fi = new FileInfo("Resources/_dbconfig.yaml");
            if (fi.Exists)
            {
                try
                {
                    using (FileStream fs = fi.OpenRead())
                    using (var sr = new StreamReader(fs, utf8))
                    {
                        yaml = sr.ReadToEnd();
                        var dsrBuilder = new DeserializerBuilder().WithNamingConvention(new UnderscoredNamingConvention());
                        var dsr = dsrBuilder.Build();
                        cfg = dsr.Deserialize<DatabaseOptions>(sr);
                    }
                } catch
                {
                    cfg = Dbconfig.Default;
                }
            }

            return new DatabaseContextBuilder(cfg.DbConfig).CreateContext();
        }
    }
}
