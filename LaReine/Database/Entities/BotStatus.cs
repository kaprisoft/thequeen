#region USING_DIRECTIVES
using Discord;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
#endregion

namespace LaReine.Database.Entities {
    public class BotStatus {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("status"), Required, MaxLength(64)]
        public string Status { get; set; }

        [Column("activity_type"), Required]
        public ActivityType Activity { get; set; } = ActivityType.Playing;
    }
}