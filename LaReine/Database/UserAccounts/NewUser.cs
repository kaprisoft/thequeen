#region USING DIRECTIVES
using System.ComponentModel.DataAnnotations.Schema;
using LaReine.Database.Entities;
#endregion

namespace LaReine.Database.UserAccounts {
    [Table("new_user_account")]
    public class NewUser {
        [NotMapped]
        public static readonly int StartingTrust = 100;
        public static readonly int StartingBal = BankAccounts.StartingBalance;

        [Column("uid")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long UserIdDb { get; set; }
        [NotMapped]
        public ulong UserId { get => (ulong)this.UserIdDb; set => this.UserIdDb = (long)value; }

        [Column("gid")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long GuildIdDb { get; set; }
        [NotMapped]
        public ulong GuildId { get => (ulong)this.GuildIdDb; set => this.GuildIdDb = (long)value; }

        [Column("trust")]
        public long Trust { get; set; } = StartingTrust;

        [Column("balance")]
        public long Balance { get; set; } = StartingBal;

        [Column("email")]
        public string EmailAddr { get; set; }

        [NotMapped]
        public DatabaseGuildConfig DbGuildConfig { get; set; }
    }
}