﻿using System;

namespace LaReine.Exceptions
{
    public class ServiceDisabledException : Exception
    {
        public ServiceDisabledException()
            : base("This service has been disabled by the bot owner.")
        {

        }
    }
}
