﻿#region USING_DIRECTIVES
using LaReine.Common;

using System;
using System.IO;

using Microsoft.Extensions.Logging;
#endregion

namespace LaReine.Logging
{
    public class LaReineLoggerProvider : ILoggerProvider
    {
        private readonly LoggingOptions _options;

        public LaReineLoggerProvider(LoggingOptions options)
        {
            _options = options;
        }

        public ILogger CreateLogger(string categoryName)
        {
            string outputDirectory = _options.UseRelativeOutput
                ? Path.Combine(AppContext.BaseDirectory, _options.OutputDirectory)
                : _options.OutputDirectory;

            return new LaReineLogger(categoryName, outputDirectory, _options.DateTimeFormat, _options.MaxFileSizeKb);
        }

        public void Dispose()
        {
            return;
        }
    }
}
