#region USING_DIRECTIVES
using Discord;
using Discord.Commands;
using System;
using System.Linq;
using System.Threading.Tasks;
using LaReine.Exceptions;
#endregion USING_DIRECTIVES

namespace LaReine.Modules.Administration {
    // User management
    [Group("user")]
    [Alias("usr", "u")]
    [Summary("Commands for managing users.")]
    public sealed class UserManagementModule : LaReineModule {
        internal Exception inner { get; private set; }

        // Kick, mute, deafen, etc.
        [Command("kick")]
        [Alias("k", "boot")]
        [Summary("Kicks the specified user from the guild.")]
        public async Task KickUserAsync(IGuildUser user, [Remainder] string reason = "No reason provided.") {
            if (user.Id == Context.Client.CurrentUser.Id) {
                await ReplyAsync("ERROR: You cannot kick yourself!");
                throw new CommandFailedException($"{user.ToString()} tried to kick themself.");
            }

            var em = new Emoji("💀");
            
            await ReplyAsync($"{user.Username} was kicked for the following reason(s): {reason}");
            await user.KickAsync();

            await InformAsync(title: "Action Report",
                              message: $"{user.Username} has been kicked from the server!", 
                              em, true);
        }

        [Command("ban")]
        [Alias("b", "bu")]
        [Summary("Bans the specified user from the guild.")]
        public async Task BanUserAsync(IGuildUser user, [Remainder] string reason = "No reason provided.") {
            if (user.Id == Context.Client.CurrentUser.Id) {
                await ReplyAsync("ERROR: You cannot ban yourself!");
                throw new CommandFailedException($"{user.ToString()} tried to ban themself.", inner);
            }

            var em = new Emoji("💀");

            await ReplyAsync($"{user.Username} has been banned from the server. ({reason})");
            await user.Guild.AddBanAsync(user, 7);
            
            await InformAsync(title: "Action Report",
                              message: $"{user.Username} has been banned from the server!", 
                              em, true);
        }

        [Command("silence")]
        [Alias("m", "mute")]
        [Summary("Mutes a user.")]
        public async Task MuteUserAsync(IGuildUser user, [Remainder] string reason = "No reason provided.") {
            var role = Context.Guild.Roles.FirstOrDefault(x => x.Name.ToString() == "Muted");
            await user.AddRoleAsync(role);

            await InformAsync("Disciplinary Action", $"{user.Mention} has been muted. ({reason})", EmojiTwo, true);
        }

        [Command("punch")]
        [Alias("deafen")]
        [Summary("Punches a user.")]
        public async Task PunchAsync(IGuildUser user, [Remainder] string reason = "Just wanted to punch someone.") {
            var role = Context.Guild.Roles.FirstOrDefault(x => x.Name.ToString() == "KO");
            await user.AddRoleAsync(role);

            await ReplyAsync($"{user.Mention} has been punched. Now they're stunned! ({reason})");
        }
    }
}