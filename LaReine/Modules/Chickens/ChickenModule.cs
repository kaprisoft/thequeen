﻿/*#region USING_DIRECTIVES

using System.Threading.Tasks;

using Discord;
using Discord.Commands;

using LaReine.Common;
using LaReine.Database;
using LaReine.Exceptions;
using LaReine.Database.Entities;
using LaReine.Extensions;
using LaReine.Modules.Chickens.Common;

#endregion

namespace LaReine.Modules.Chickens
{
    [Group("chicken")]
    [Summary("Manage your chicken. If invoked without subcommands, prints out your chicken information.")]
    [Alias("chick", "hen", "cc")]
    class ChickenModule : LaReineModuleBase
    {
        public ChickenModule(DatabaseContextBuilder dbb)
            : base(dbb)
        {
            this.ModuleColor = Color.Gold;
        }

        [Command]
        public Task ExecuteGroupAsync(CommandContext ctx,
                                      [Summary("User.")] IDiscordClient member)
            => this.InfoAsync(ctx, member.CurrentUser);

        public async Task InfoAsync(CommandContext ctx,
                                   [Description("Emoji.")] Emoji emoji)
        {
            Emoji gemoji = await ctx.Guild.GetEmojiAsync(emoji.Id);

            var emb = new EmbedBuilder()
            {
                Title = "Emoji details:",
                Description = gemoji,
                Color = this.ModuleColor,
                ThumbnailUrl = gemoji.Url
            };

            emb.AddField("Name", Formatter.InlineCode(gemoji.Name), inline: true);
            emb.AddField("Created by", gemoji.User is null ? "<unknown>" : gemoji.User.Username, inline: true);
            emb.AddField("Integration managed", gemoji.IsManaged.ToString(), inline: true);

            await ctx.RespondAsync(embed: emb.Build());
        }
    }
}
*/