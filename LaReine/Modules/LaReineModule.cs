#region Using Directives
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using LaReine.Database;
#endregion

namespace LaReine.Modules {
    public abstract class LaReineModule : ModuleBase<SocketCommandContext> {

        protected static readonly HttpClient _http;
        private static readonly HttpClientHandler _handler;

        internal CommandContext Ctx { get; private set; }
        internal DatabaseContextBuilder Database { get; set; }
        internal Color ModuleColor {
            get { return this.moduleColor ?? Color.Green; }
            set { this.moduleColor = value; }
        }
        internal Emoji EmojiOne = new Emoji("✔");
        internal Emoji EmojiTwo = new Emoji("✅");

        private Color? moduleColor;

        static LaReineModule() {
            _handler = new HttpClientHandler {
                AllowAutoRedirect = true
            };

            _http = new HttpClient(_handler, true);
        }

        protected LaReineModule(DatabaseContextBuilder dbb = null) {
            this.Database = dbb;
            this.ModuleColor = Color.Green;
        }
        
        protected Task InformAsync(string title = null, string message = null, string emoji = null, bool isImportant = false)
            => this.InformAsync(title, message, (emoji is null ? EmojiOne : EmojiTwo), isImportant);

        protected async Task InformAsync(string title = null, string message = null, Emoji emoji = null, bool isImportant = false) {
            if (!isImportant) {
                try {
                    await Context.Message.AddReactionAsync(EmojiOne);
                } catch {
                    await this.InformAsync("Action Report:", "Command completed!", EmojiOne, false);
                }
            } else {
                await Context.Channel.SendMessageAsync(embed: new EmbedBuilder {
                    Title = title,
                    Color = Color.Gold,
                    Description = $"{emoji ?? EmojiOne} {message}"
                }.Build());
            }
        }

        protected Task InformFailureAsync(string title = null, string message = null) {
            var em = new Emoji("❎");

            return ReplyAsync(null, false, new EmbedBuilder {
                Title = title,
                Color = Color.Gold,
                Description = $"{em} {message}"
            }.Build());
        }

        protected async Task<bool> IsValidImageUriAsync(Uri uri) {
            try {
                HttpResponseMessage response = await _http.GetAsync(uri).ConfigureAwait(false);
                if (response.Content.Headers.ContentType.MediaType.StartsWith("image/"))
                    return true;
            } catch {

            }

            return false;
        }
    }
}