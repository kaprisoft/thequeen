﻿#region USING_DIRECTIVES
using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using LaReine.Modules;
#endregion

namespace LaReine.Modules.Public
{

    [Group("help")]
    [Summary("Help dialogue.")]
    public sealed class HelpModule : LaReineModule {

        private string DeleteDescriptor { get; set; }
        private string PruneDescriptor { get; set; }
        private string HelpDescriptor { get; set; }

        public HelpModule() {
            DeleteDescriptor = "Deletes the specified number of messages.";
            PruneDescriptor = "Recursively deletes 100 messages. (A different number can be supplied)";
            HelpDescriptor = "Shows a list of extra commands for fun.";
        }

        // >help info
        [Command("info")]
        [Alias("about", "whoami", "owner")]
        public async Task InfoAsync() {
            var app = await Context.Client.GetApplicationInfoAsync();

            await ReplyAsync(
                $"La Reine is a private-use Discord bot created for use within the Kaprisoft Network server.\n\n" +
                $"{Format.Bold("Info")}\n" +
                $"- Author: {app.Owner} ({app.Owner.Id})\n" +
                $"- Library: Discord.Net ({DiscordConfig.Version})\n" +
                $"- Runtime: {RuntimeInformation.FrameworkDescription} {RuntimeInformation.ProcessArchitecture} " +
                    $"({RuntimeInformation.OSDescription} {RuntimeInformation.OSArchitecture})\n" +
                $"- Uptime: {GetUptime()}\n\n" +

                $"{Format.Bold("Stats")}\n" +
                $"- Heap Size: {GetHeapSize()}MiB\n" +
                $"- Guilds: {Context.Client.Guilds.Count}\n" +
                $"- Channels: {Context.Client.Guilds.Sum(g => g.Channels.Count)}\n" +
                $"- Users: {Context.Client.Guilds.Sum(g => g.Users.Count)}\n");
        }

        private static string GetUptime() => (DateTime.Now - Process.GetCurrentProcess().StartTime).ToString(@"dd\.hh\:mm\:ss");
        private static string GetHeapSize() => Math.Round(GC.GetTotalMemory(true) / (1024.0 * 1024.0), 2).ToString();

        [Command]
        public async Task Help() {

            var emb = new EmbedBuilder { 
                Title = "La Reine de la Discord <-> HELP",
                Description = $"{Format.Bold("Group- PUBLIC:")}\n" +
                               "\n" +
                              $"{Format.Underline("Message-related")}\n" +
                               "\n" +
                              $"/delete <(OPTIONAL) Number of Messages>: {DeleteDescriptor}\n" + 
                              $"/prune <(OPTIONAL) Number of Messages>: {PruneDescriptor}\n" +
                               "\n" +
                              $"{Format.Underline("Fun / Extra")}\n" +
                               "\n" +
                              $"/owo: {HelpDescriptor}\n" +
                               "\n" +
                              $"{Format.Italics("More commands coming soon!")}",
                Color = Color.Gold,
                Url = "https://gitlab.com/kaprisoft/thequeen"
            }.Build();

            await ReplyAsync("", false, emb);
        }

        [Command("userinfo")]
        [Summary("Returns info about the current user, or the user parameter, if one passed.")]
        [Alias("user", "whois")]
        public async Task UserInfoAsync(
            [Summary("The (optional) user to get info from")]
            SocketUser user = null) {
            var userInfo = user ?? Context.Client.CurrentUser;
            await ReplyAsync($"{userInfo.Username}#{userInfo.Discriminator} ({userInfo.Mention})");
        }
    }
}
