﻿#region USING_DIRECTIVES
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Threading.Tasks;
#endregion

namespace LaReine.Modules.Public {

    [Group("delete")]
    [Alias("clear", "del")]
    [Summary("Used to delete messages.")]
    [RequireUserPermission(GuildPermission.Administrator)]
    [RequireBotPermission(GuildPermission.ManageMessages)]
    public sealed class DeleteModule : LaReineModule {

        // /delete
        [Command]
        public async Task Default(int count = 10)
            => await Messages(count);

        // /delete messages 15
        [Command("messages")]
        public async Task Messages(int count)
        {
            var messages = await (Context.Channel as SocketTextChannel).GetMessagesAsync(count + 1).FlattenAsync();

            await (Context.Channel as SocketTextChannel).DeleteMessagesAsync(messages);
            await Task.Delay(TimeSpan.FromSeconds(3));

            var m = await ReplyAsync($"Number of messages deleted: {count}");
            await m.DeleteAsync();
        }
    }

    [Group("prune")]
    [Alias("purge")]
    [Summary("Used for deleting messages older than two weeks.")]
    [RequireUserPermission(GuildPermission.Administrator)]
    [RequireBotPermission(GuildPermission.ManageMessages)]
    public sealed class PruneModule : LaReineModule {

        [Command]
        public async Task Prune(int count = 100) {
            var messages = await (Context.Channel as SocketTextChannel).GetMessagesAsync(count).FlattenAsync();
            foreach (IUserMessage message in messages) {
                await message.DeleteAsync();
            }

            await Task.Delay(TimeSpan.FromSeconds(3));

            var m = await ReplyAsync($"Number of messages deleted: {count}");
            await m.DeleteAsync();
        }
    }
}
