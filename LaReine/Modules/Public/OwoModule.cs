﻿#region USING_DIRECTIVES

using Discord;
using Discord.Commands;
using Discord.WebSocket;

using System;
using System.Threading.Tasks;
using System.Collections.Generic;
#endregion

namespace LaReine.Modules.Public {
    [Group("owo")]
    [Summary("A collection of commands for fun. :)")]
    [RequireUserPermission(GuildPermission.SendMessages)]
    public sealed class OwoModule : LaReineModule {

        public readonly IDiscordClient Client;

        public OwoModule(DiscordSocketClient discord) {
            this.Client = discord;
        }

        [Command]
        public async Task OwoHelp() {
            string CelebrateDescriptor = "Sometimes you've just gotta celebrate the good times.";
            string HugDescriptor = "Sometimes, people just need a hug.";
            string KillDescriptor = "For dealing with your enemies! >:^)";
            string SlapDescriptor = "Sometimes people just need a hard smack to teach 'em who's boss.";
            string CookieDescriptor = "Give a cookie to your friends! (or foes!) >:^D";
            string WowDescriptor = "Wow. J u s t W O W...!";

            var emb = new EmbedBuilder { 
                Title = "OwO Commands - Help",
                Description = $"{Format.Bold("OwO")}\n" +
                               "\n" +
                              $"/owo celebrate: {CelebrateDescriptor}\n" +
                              $"/owo hug: {HugDescriptor}\n" +
                              $"/owo kill: {KillDescriptor}\n" +
                              $"/owo slap: {SlapDescriptor}\n" +
                              $"/owo cookie: {CookieDescriptor}\n" +
                              $"/owo wow: {WowDescriptor}\n" +
                               "\n" +
                              $"{Format.Italics("More OwO commands coming soon!")}",
                Color = Color.Gold
            }.Build();

            await ReplyAsync("", false, emb);
        }

        [Command("celebrate", RunMode = RunMode.Async)]
        [Alias("party", "hooray")]
        [Summary("Sometimes you just need to celebrate the good times.")]
        public async Task Celebrate() {
            Random rnd = new Random();
            int whichOne = rnd.Next(1, 4);

            if (whichOne == 1) {
                await ReplyAsync("Celebration incoming! :D");
                await Task.Delay(TimeSpan.FromSeconds(5));
                await ReplyAsync("", false, new EmbedBuilder { 
                    Title = "Here's your celebration!",
                    ImageUrl = "https://media.giphy.com/media/l0MYt5jPR6QX5pnqM/giphy.gif"
                }.Build());
            }

            if (whichOne == 2) {
                await ReplyAsync("Celebration incoming! :D");
                await Task.Delay(TimeSpan.FromSeconds(5));
                await ReplyAsync("", false, new EmbedBuilder {
                    Title = "Here's your celebration!",
                    ImageUrl = "https://media.giphy.com/media/uM2nSR78R2sgM/giphy.gif"
                }.Build());
            }

            if (whichOne == 3) {
                await ReplyAsync("Celebration incoming! :D");
                await Task.Delay(TimeSpan.FromSeconds(5));
                await ReplyAsync("", false, new EmbedBuilder {
                    Title = "Here's your celebration!",
                    ImageUrl = "https://media.giphy.com/media/6G8luKUp4dt1LpywIg/giphy.gif"
                }.Build());
            }

            if (whichOne == 4) {
                await ReplyAsync("Celebration incoming! :D");
                await Task.Delay(TimeSpan.FromSeconds(5));
                await ReplyAsync("", false, new EmbedBuilder { 
                    Title = "Here's your celebration!",
                    ImageUrl = "https://media.giphy.com/media/6nuiJjOOQBBn2/giphy.gif"
                }.Build());
            }

            await Task.Delay(TimeSpan.FromSeconds(15));
            await ReplyAsync("Alright, party's over! Back to work!");
        }

        [Command("hug")]
        [Alias("h")]
        [Summary("Sometimes people just need a hug.")]
        public async Task Hug(SocketUser user) {
            if (user == null) {
                await ReplyAsync($"{Context.User.Mention} is lonely. Somebody hug them!");

                return;
            }

            var emoji = new Emoji("❤️");

            await ReplyAsync(null, false, new EmbedBuilder {
                Title = "A HUG!",
                Description = $"{Context.User.Mention} hugs {user.Mention}! Awww... {emoji}",
                ImageUrl = "https://media.giphy.com/media/EvYHHSntaIl5m/giphy.gif",
                Color = Color.Red
            }.Build());
        }

        [Command("kill")]
        [Alias("murder")]
        [Summary("Use this to deal with your enemies! >:D")]
        public async Task Kill(SocketUser user) {
            if (user == null) {
                user = Context.User;

                await ReplyAsync($"{user.Mention}, you cannot kill yourself! :^)");
            }

            var emoji = new Emoji("☠️");

            var image = await ReplyAsync(null, false, new EmbedBuilder {
                Title = "A KILLER!",
                Description = $"{Context.User.Mention} killed {user.Mention}! {emoji}",
                ImageUrl = "https://media.giphy.com/media/xT1XGBamT4gKtRfCfe/giphy.gif",
                Color = Color.Gold
            }.Build());
            await Task.Delay(TimeSpan.FromSeconds(15));
            await image.DeleteAsync();
        }

        [Command("smack")]
        [Alias("slap", "hit")]
        [Summary("Use to smack your friends around! >:^)")]
        public async Task Smack(SocketUser user) {
            if (user == null) {
                await ReplyAsync($"{Context.User.Mention}, you cannot smack yourself!");
                
                return;
            }

            var emoji = new Emoji("👏");
            var emb = new EmbedBuilder()
                      .WithTitle("Slap initiated!")
                      .WithDescription($"{user.Mention}, you've been slapped! {emoji}")
                      .WithImageUrl("https://media.giphy.com/media/3XlEk2RxPS1m8/giphy.gif") // Reimplement with link to 'slapping' GIF
                      .Build();
            await ReplyAsync(null, false, emb);
        }

        [Command("cookie")]
        [Summary("Give a cookie to a friend! (Or a poisoned cookie to a foe! >:D)")]
        public async Task Cookie(SocketUser user, [Remainder] string deliveryNote = "Here's a cookie!") {
            if (user == null) {
                user = Context.User;

                await ReplyAsync($"{user.Mention}, you cannot give yourself a cookie!");
                return;
            }

            var emoji = new Emoji("🍪");
            var emb = new EmbedBuilder { 
                Title = "A COOKIE!",
                Description = $"{user.Mention}, {deliveryNote} +1! {emoji}",
                ImageUrl = "https://media.giphy.com/media/3oz8xK0liGg1yTgcZq/giphy.gif",
                Color = Color.Gold
            }.Build();

            var m = await ReplyAsync("", false, emb);
            await Task.Delay(TimeSpan.FromSeconds(15));
            await m.DeleteAsync();
        }

        [Command("wow")]
        [Summary("Wow!")]
        public async Task Wow() { 
            await ReplyAsync("", false, new EmbedBuilder { 
                Title = ":O",
                ImageUrl = "https://media.discordapp.net/attachments/440133432396218380/551573014865772594/IMG_20181024_064023.jpg",
                Color = Color.Green
            }.Build());
        }
    }
}
