﻿#region USING_DIRECTIVES
using System.Reflection;
using System.Diagnostics;
using System.Threading.Tasks;
#endregion

namespace LaReine
{
    internal class Program
    {
        internal static Task Main(string[] args)
        {
            return new Startup(args).RunAsync();
        }
    }
}
