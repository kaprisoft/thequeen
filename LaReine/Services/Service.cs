﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LaReine.Services
{
    public abstract class Service
    {
        public abstract void Start();
        public abstract Task Stop();
    }
}
