﻿#region USING_DIRECTIVES

using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using Discord;
using Discord.Rest;
using Discord.Commands;
using Discord.WebSocket;

using LaReine.Common;
using LaReine.Logging;
using LaReine.Modules.Readers;

#endregion

namespace LaReine.Services
{
    public sealed class StartupService : Service
    {
        private readonly ILoggerFactory _loggerFactory;
        private readonly IServiceProvider _provider;
        private readonly IConfiguration _config;
        private readonly DiscordSocketClient _discord;
        private readonly CommandService _commands;

        public StartupService(
            ILoggerFactory loggerFactory,
            IServiceProvider provider,
            IConfiguration config,
            DiscordSocketClient discord,
            CommandService commands)
        {
            _loggerFactory = loggerFactory;
            _provider = provider;
            _config = config;
            _discord = discord;
            _commands = commands;
        }

        public override void Start()
            => StartAsync().ConfigureAwait(false).GetAwaiter().GetResult();
        public override Task Stop() {
            return Task.CompletedTask; 
        }

        public async Task StartAsync()
        {
            var fileLoggerOptions = new LoggingOptions();
            _config.Bind("filelogger", fileLoggerOptions);
            _loggerFactory.AddProvider(new LaReineLoggerProvider(fileLoggerOptions));

            await _discord.LoginAsync(TokenType.Bot, _config["discord:token"]);
            await _discord.StartAsync();

            var guildReader = new GuildTypeReader();
            _commands.AddTypeReader<IGuild>(guildReader);
            _commands.AddTypeReader<RestGuild>(guildReader);
            _commands.AddTypeReader<SocketGuild>(guildReader);
            _commands.AddTypeReader<Uri>(new UriTypeReader());

            await _commands.AddModulesAsync(Assembly.GetEntryAssembly(), _provider);
        }
    }
}
