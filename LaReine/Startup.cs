﻿#region USING_DIRECTIVES
using System;
using System.IO;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Discord;
using Discord.Commands;
using Discord.WebSocket;

using YamlDotNet;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

using LaReine.Common;
using LaReine.Services;
#endregion

namespace LaReine
{
    public sealed class Startup
    {
        public IConfiguration Configuration { get; }
        public CancellationTokenSource Cts { get; }

        private static readonly string AppName = "La Reine de la Discord";
        private static readonly string Version = "v1.0.0-SNAPSHOT";

        public Startup(string[] args)
        {
            this.Cts = new CancellationTokenSource();

            TryGenerateConfig();

            var builder = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddYamlFile("_config.yaml")
                .Build();
            Configuration = builder;
        }

        public static bool TryGenerateConfig()
        {
            var filepath = Path.Combine(AppContext.BaseDirectory, "_config.yaml");
            if (File.Exists(filepath)) return false;

            var serializer = new SerializerBuilder()
                .WithNamingConvention(new UnderscoredNamingConvention())
                .Build();

            var yaml = serializer.Serialize(new AppOptions(true));
            File.WriteAllText(filepath, yaml);
            return true;
        }

        public async Task RunAsync()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var fileVersionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);

            Console.WriteLine($"{AppName} {Version} ({fileVersionInfo.FileVersion})");

            var services = new ServiceCollection();
            ConfigureServices(services);
            var provider = services.BuildServiceProvider();

            provider.GetRequiredService<CommandHandler>().Start();
            provider.GetRequiredService<LoggingService>().Start();
            provider.GetRequiredService<StartupService>().Start();

            await Task.Delay(Timeout.Infinite, Cts.Token);
        }

        private void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(new DiscordSocketClient(new DiscordSocketConfig
            {
                LogLevel = LogSeverity.Verbose,
                MessageCacheSize = 1000
            }))
            .AddSingleton(new CommandService(new CommandServiceConfig 
            { 
                DefaultRunMode = RunMode.Async,
                CaseSensitiveCommands = false,
                LogLevel = LogSeverity.Verbose,
                IgnoreExtraArgs = false
            }))
            .AddSingleton<CommandHandler>()
            .AddSingleton<StartupService>()
            .AddSingleton<LoggingService>()
            .AddLogging()
            .AddSingleton<Random>()
            .AddSingleton(Configuration);
        }
    }
}
